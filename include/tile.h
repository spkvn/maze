#ifndef TILE_H
#define TILE_H

#include "coordinate.h"
#include <string>

class Tile : public Coordinate {
public:
    // getters setters.
    bool isWall();
    bool isNotWall();
    void setWall(bool isWall);
    Tile();
    Tile(int aX, int aY, bool isWall);
    std::string print();
private:
    bool wall;
};
#endif

