#ifndef COORDINATE_H
#define COORDINATE_H

class Coordinate {
private:
    int X;
    int Y;
public:
    Coordinate();
    Coordinate(int anX, int anY);

    void setX(int anX);
    void setY(int anY);
    int getX();
    int getY();
};

#endif //MAZE_COORDINATE_H
