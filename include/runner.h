#ifndef RUNNER_H
#define RUNNER_H

#include <string>

#include "coordinate.h"

class Runner : public Coordinate {
public:
    enum Heading {NORTH, EAST, SOUTH, WEST};

    // constructor, getters, setters
    Runner(int aX, int aY, Heading aHeading);

    void setHeading(Heading aHeading);
    Heading getHeading();

    void setCoordinate(int aX, int aY);

    std::string print();
private:
    Coordinate position;
    Heading heading;
    
};
#endif