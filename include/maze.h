#ifndef MAZE_H
#define MAZE_H

#include <string>
#include <iostream>
#include <time.h>

#include "tile.h"
#include "../src/two_dim_vec.cpp"
#include "runner.h"

class Maze {
protected:
    int X;
    int Y;
    TwoDimensionVector<Tile> map;
    Runner runner;

    Tile getRunnersLeft();
    Tile getRunnersRight();
    Tile getRunnersAhead();
    Tile getRunnersBehind();

    Tile getTileOrCreateImplicitWall(int aX, int aY);

public:
    Maze(int aX, int aY);
    void print();
//    virtual void move();

    void setX(int aX);
    int getX();
    void setY(int aY);
    int getY();
};
#endif