#ifndef TWO_DIM_VEC_H
#define TWO_DIM_VEC_H

#include <vector>
#include <iostream>
#include <invalid_coordinate_exception.h>

template <class T>
class TwoDimensionVector {
private:
    int x; // x = horizontal = number columns
    int y; // y = vertical = number rows in a col.
    std::vector<std::vector<T> > vector;
public:
    int getX();
    void setX(int aX);
    
    int getY();
    void setY(int aY);

    T getElementAt(int aX, int aY); 
    void setElementAt(int aX, int aY, T element);

    TwoDimensionVector(int aX, int aY);
};
#endif 
