#ifndef FOLLOW_LEFT_MAZE_H
#define FOLLOW_LEFT_MAZE_H

#include "maze.h"
#include "invalid_coordinate_exception.h"

class FollowLeftMaze : public Maze {
    using Maze::Maze;
public:
    void move();

};


#endif //MAZE_FOLLOW_LEFT_MAZE_H