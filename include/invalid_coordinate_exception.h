#include <exception>
#include <cstdio>
#include "coordinate.h"

#ifndef INVALID_COORDINATE_EXCEPTION_H
#define INVALID_COORDINATE_EXCEPTION_H

class InvalidCoordinateException: public std::exception
{
private:
    Coordinate invalidCoordinate;
public:
    InvalidCoordinateException(Coordinate anInvalidCoordinate) {
        invalidCoordinate = anInvalidCoordinate;
    }
    InvalidCoordinateException(int X, int Y) {
        invalidCoordinate = Coordinate(X,Y);
    }
    virtual const char* what() {
        char what[256];
        sprintf(what, "No element exists at x:%d, y:%d", invalidCoordinate.getX(),invalidCoordinate.getY());
        return what;
    }
};

#endif //MAZE_INVALID_COORDINATE_EXCEPTION_H
