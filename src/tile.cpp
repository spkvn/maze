#include "../include/tile.h"

Tile::Tile(int aX, int aY, bool isWall) {
    setX(aX);
    setY(aY);
    setWall(isWall);
}

Tile::Tile() {
    setX(0);
    setY(0);
    setWall(false);
}

void Tile::setWall(bool aWall) {
    wall = aWall;
}

bool Tile::isWall() {
    return wall;
}
bool Tile::isNotWall(){
    return !isWall();
}

std::string Tile::print() {
    return isWall() ? "#" : " ";
}
