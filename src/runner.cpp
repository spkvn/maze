#include "../include/runner.h"

Runner::Runner(int aX, int aY, Runner::Heading aHeading) {
    setX(aX);
    setY(aY);
    setHeading(aHeading);
}

void Runner::setHeading(Runner::Heading aHeading) {
    heading = aHeading;
}
Runner::Heading Runner::getHeading(){
    return heading;
}

std::string Runner::print() {
    std::string out = "←";
    if (heading == Runner::NORTH) {
        out = "↑";
    } else if (heading == Runner::EAST) {
        out = "→";
    } else if (heading == Runner::SOUTH) {
        out = "↓";
    }

    return out;
}

void Runner::setCoordinate(int aX, int aY) {
    setX(aX);
    setY(aY);
}
