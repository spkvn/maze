#include "../include/maze.h"

void Maze::setX(int aX) {
	X = aX;
}

int Maze::getX() {
	return X;
}

void Maze::setY(int aY) {
	Y = aY;
}

int Maze::getY() {
	return Y;
}

Maze::Maze(int aX, int aY) : map(0,0), runner(0,0,Runner::SOUTH) {
	setX(aX);
	setY(aY);
	map.setY(aY);
	map.setX(aX);
    srand(time(0));
	for(int x = 0; x < X; x++) {
		for(int y = 0; y < Y; y++) {
			bool wally = rand() & 1;
			map.setElementAt(x,y, Tile(x, y, wally));
		}
	}
	// runner = Runner(0,0,Runner::NORTH);
}

void Maze::print() {
	std::string mazeString;
	for ( int x = 0; x < X; x++ ) {
		for ( int y = 0; y < Y; y++ ){
			if(x == runner.getX() && y == runner.getY()) {
				mazeString += runner.print();
			} else if(map.getElementAt(x,y).isWall()) {
				mazeString += "#";
			} else {
				mazeString += " ";
			}
		}
		mazeString += "\n";
	}
	std::cout << mazeString << std::endl;
}


Tile Maze::getRunnersAhead() {
    Runner::Heading heading = runner.getHeading();
    int rX = runner.getX();
    int rY = runner.getY();
    int destX = rX;
    int destY = rY;

    switch (heading) {
        case Runner::SOUTH:
            destY++;
            break;
        case Runner::EAST:
            destX++;
            break;
        case Runner::WEST:
            destX--;
            break;
        case Runner::NORTH:
            destY--;
            break;
    }

    return getTileOrCreateImplicitWall(destX, destY);
}
Tile Maze::getRunnersBehind(){
    Runner::Heading heading = runner.getHeading();
    int rX = runner.getX();
    int rY = runner.getY();
    int destX = rX;
    int destY = rY;

    switch (heading) {
        case Runner::SOUTH:
            destY--;
            break;
        case Runner::EAST:
            destX--;
            break;
        case Runner::WEST:
            destX++;
            break;
        case Runner::NORTH:
            destY++;
            break;
    }

    return getTileOrCreateImplicitWall(destX, destY);
}
Tile Maze::getRunnersLeft(){
    Runner::Heading heading = runner.getHeading();
    int rX = runner.getX();
    int rY = runner.getY();
    int destX = rX;
    int destY = rY;

    switch (heading) {
        case Runner::SOUTH:
            destX++;
            break;
        case Runner::EAST:
            destY++;
            break;
        case Runner::WEST:
            destY++;
            break;
        case Runner::NORTH:
            destX--;
            break;
    }
    return getTileOrCreateImplicitWall(destX, destY);
}
Tile Maze::getRunnersRight(){    Runner::Heading heading = runner.getHeading();
    int rX = runner.getX();
    int rY = runner.getY();
    int destX = rX;
    int destY = rY;

    switch (heading) {
        case Runner::SOUTH:
            destX--;
            break;
        case Runner::EAST:
            destY++;
            break;
        case Runner::WEST:
            destY--;
            break;
        case Runner::NORTH:
            destX++;
            break;
    }

    return getTileOrCreateImplicitWall(destX, destY);
}

Tile Maze::getTileOrCreateImplicitWall(int aX, int aY) {
    try {
        return map.getElementAt(aX,aY);
    } catch (InvalidCoordinateException& invalidException) {
        return Tile(aX,aY,true) ;
    }
}