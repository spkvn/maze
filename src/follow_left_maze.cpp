#include "../include/follow_left_maze.h"

void FollowLeftMaze::move() {

    Tile left = getRunnersLeft();
    Tile right = getRunnersRight();
    Tile ahead = getRunnersAhead();
    Tile behind = getRunnersBehind();

    int rX, rY = 0;
    rX = runner.getX();
    rY = runner.getY();


    printf("Runner %s : x:%d,y:%d\n",runner.print().c_str(), rX, rY);
    printf("Runner's ahead: x:%d,y:%d, [%s]\n", ahead.getX(), ahead.getY(), ahead.print().c_str());
    printf("Runner's behind: x:%d,y:%d [%s]\n", behind.getX(), behind.getY(), behind.print().c_str());
    printf("Runner's left: x:%d,y:%d [%s]\n", left.getX(), left.getY(), left.print().c_str());
    printf("Runner's right: x:%d,y:%d [%s]\n", right.getX(), right.getY(), right.print().c_str());

    // if left of runner is wally, check the front, else go left.
    // if front of runner is wally, check the right, else go front.
    // if right of runner is wally, check behind, else go right
    // if behind of runner is wally, don't move, you're trapper, else go behind.
    if(left.isNotWall()) {
        runner.setCoordinate(left.getX(), left.getY());
    } else if (ahead.isNotWall()) {
        runner.setCoordinate(ahead.getX(),ahead.getY());
    } else if (right.isNotWall()) {
        runner.setCoordinate(right.getX(), right.getY());
    } else if (behind.isNotWall()) {
        runner.setCoordinate(behind.getX(),right.getY());
    }
}