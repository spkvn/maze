#include "../include/coordinate.h"

Coordinate::Coordinate(){
    setX(0);
    setY(0);
}

Coordinate::Coordinate(int anX, int anY) {
    setX(anX);
    setY(anY);
}

int Coordinate::getX(){
    return X;
}

int Coordinate::getY() {
    return Y;
}

void Coordinate::setY(int anY) {
    Y = anY;
}
void Coordinate::setX(int anX) {
    X = anX;
}
