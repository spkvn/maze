#include <iostream>

#include "../include/follow_left_maze.h"
#include <unistd.h>

// thanks: Cat Plus Plus
// https://stackoverflow.com/a/6487534/3740127
void clear() {
    // CSI[2J clears screen, CSI[H moves the cursor to top-left corner
    std::cout << "\x1B[2J\x1B[H";
}

int main(int argc, char** argv) {
    FollowLeftMaze m = FollowLeftMaze(5,10);
    int sleepMilliseconds = 400 * 1000;
    bool complete = false;
    while (complete == false) {
        clear();
        m.print();
        m.move();
        // complete = m.isComplete();
        usleep(sleepMilliseconds);
    }

}