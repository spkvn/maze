#include "../include/two_dim_vec.h"

template <class T>
TwoDimensionVector<T>::TwoDimensionVector(int aX, int aY) {
    setY(aY);
    setX(aX);
}

template <class T>
int TwoDimensionVector<T>::getX() {
    return x;
}

template <class T>
void TwoDimensionVector<T>::setX(int aX) {
    x = aX;
    for(int i = 0; i < y; i++ ){
        vector[i].resize(x);
    }
}

template <class T>
int TwoDimensionVector<T>::getY() {
    return y;
}

template <class T>
void TwoDimensionVector<T>::setY(int aY) {
    y = aY;
    vector.resize(y);
}

template <class T>
T TwoDimensionVector<T>::getElementAt(int aX, int aY){
    if (aX > x || aX < 0 || aY > y || aY < 0) {
        throw InvalidCoordinateException(aX,aY);
    }
    return vector[aX][aY];
}

template <class T>
void TwoDimensionVector<T>::setElementAt(int aX, int aY, T element) {
    vector[aX][aY] = element;
}
